﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Õpilaste_ülesanne
{
    class Program
    {
        static void Main(string[] args)
        {
   
                foreach (var rida in File.ReadAllLines(@"..\..\Lapsevanemad.txt")
                    .Select(x => x.Trim())
                    .Where(x => x.Length > 0)
                    .Select(x => x.Split(',')).ToArray())
                    {
                    Inimene i = Inimene.Create(rida[0]);
                    i.Eesnimi = rida[1];
                    i.Perekonnanimi = rida[2];
                    i.Laps = rida[3];
                    }
                foreach (var rida in File.ReadAllLines(@"..\..\Lapsed.txt")
                    .Select(x => x.Trim())
                    .Where(x => x.Length > 0)
                    .Select(x => x.Split(',')).ToArray())
                    {
                    Inimene i = Inimene.Create(rida[0]);
                    i.Eesnimi = rida[1];
                    i.Perekonnanimi = rida[2];
                    i.Klass = rida[3];
                    }
 
                foreach (var rida in File.ReadAllLines(@"..\..\Õpetajad.txt")
                    .Select(x => x.Trim())
                    .Where(x => x.Length > 0)
                    .Select(x => x.Split(',')).ToArray())
                    {
                    Inimene i = Inimene.Create(rida[0]);
                    i.Eesnimi = rida[1];
                    i.Perekonnanimi = rida[2];
                    i.Aine = rida[3];
                    }

            //string[] hinneteMassiiv = File.ReadAllLines(@"..\..\Hinded.txt");
            //foreach (var x in hinneteMassiiv)
            //    Console.WriteLine(x);

                string vastus;
            do
            {
                Console.Write("Sisesta isikukood: ");
                vastus = Console.ReadLine();
                if (vastus != "")
                {
                    Inimene kes = Inimene.ByCode(vastus);
                    if (kes != null)
                    {
                        if (kes.KasÕpetaja) {Console.WriteLine("Tere õpetaja. Siin peaks olema sinu nimi. Kahjuks ma ei oska seda kuvada."); Console.WriteLine(kes);} 
                    
                        Console.Write("Vali tegevus - (x) Välju programmist, (k) Kuva minu klasside nimekirjad, (6) Kuva minu ainete hinded ");
                        switch (Console.ReadLine().Substring(0, 1).ToLower())
                        {
                            case "x":
                                return; // tsükkel (programm) peakski siin loogiliselt lõppema (vt while tsükli lõppu), see on kõige õigem väljumise viis. Meetodi sees saab kasutada ka returni, funktsiooni sees peab return midagi tagastama. 
                            case "k":
                                Console.WriteLine("Tahad näha klasse kus sa õpetad. Ma kahjuks ei oska neid sulle kuvada");
                                break;
                            case "6":
                                Console.WriteLine("Tahad näha mis hindeid sa oled õpilastele pannud oma ainetes. Kahjuks ma ei oska seda sulle näidata");
                                break;
                        }
                    }

                    else if (kes != null)
                    {
                        if (kes.KasÕpilane) { Console.WriteLine("Tere õpilane"); Console.WriteLine(kes); };

                        Console.Write("Vali tegevus - (x) Välju programmist, (n) Kuva minu klassi nimekiri, (a) Kuva minu ained, (h) Kuva minu hinded");
                        switch (Console.ReadLine().Substring(0, 1).ToLower())
                        {
                            case "x":
                                return;
                            case "n":
                                Console.WriteLine("Kõik õpilased on siin: ");
                                foreach (var i in Inimene.Inimesed.Values.Where(x => x.KasÕpilane))
                                    Console.WriteLine(i);

                                Console.WriteLine("Tahad näha oma klassikaaslaste nimekirja. Ma kahjuks ei oska seda sulle kuvada");
                                break;
                            case "a":
                                Console.WriteLine("Tahad näha oma õppeaineid - ei saa kahjuks, ma ei oska kuvada");
                                break;
                            case "h":
                                Console.WriteLine("Tahad näha oma hindeid. Kahjuks ma ei oska neid sulle kuvada");
                                break;
                        }
                    }

                    else if (kes != null)
                    {
                        if (kes.KasLapsevanem) { Console.WriteLine("Tere lapsevanem"); Console.WriteLine(kes); };

                        Console.Write("Vali tegevus - (x) Välju programmist, (l) Kuva minu lapse hinded, (s) Kuva minu lapse klassi nimekiri, (v) Kuva minu lapse klassi vanemad");
                        switch (Console.ReadLine().Substring(0, 1).ToLower())
                        {
                            case "x":
                                return;
                            case "l":
                                Console.WriteLine("Tahad näha oma lapse hindeid. Ära hakka ennast tühja asja pärast ärritama. Ma niikuinii ei oska seda sulle kuvada. ");
                                break;
                            case "s":
                                Console.WriteLine("Tahad näha kes on su lapse klassikaaslased. Milleks asjata uudishimutseda? Ma ei oska näidata sulle seda");
                                break;
                            case "v":
                                Console.WriteLine("Tahad näha lapsevanemate andmeid sinu lapse klassis. See vist pole küll isikuandmete kaitse seisukohast lubatud. Ma ei oska kuvada ka");
                                break;
                        }
                    }
                    //string vali = "Vali tegevus - (x) lõpetamine";
                    //if (kes.KasÕpilane) vali += "õpilasele mõeldud asjad (n - klassi nimekiri e. kes õpivad sisselogitud inimesega samas klassis, a-  klassi ained kes.Klass.Ained, h - oma hinded ained e. hinded ainete kaupa kus õpilane on kes)";
                    //if (kes.KasÕpetaja) vali += "õpetajale mõeldud asjad (õ - klassi nimekiri - 1) teed loetelu klassidest mida näidata ja  2) küsid milise klassi nimekirja vaja, nh - nimekiri koos hinnetega)";

                    //if (kes.KasLapseVanem) vali += "lapsevanemale mõeldud asjad (l - laste hinded - teeb loetelu lastest ja siis küib millise hindeid tahab, k - klassikaaslased - , v - klassikaaslaste vanemad";

                    else
                    {
                        Console.WriteLine("Sellist isikukoodi pole olemas!");
                    }

                }
            } while (vastus != "");
        }
    }
}
