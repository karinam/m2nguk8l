﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Õpilaste_ülesanne

{
    public class Inimene
    {
        public readonly String Isikukood;
        public static Dictionary<string, Inimene> Inimesed = new Dictionary<string, Inimene>();

        protected Inimene(string isikukood)
        {
            Isikukood = isikukood;
            Inimesed.Add(isikukood, this);
        }
        //Kontrollib kas isikukood on olemas (nt kui õpetaja on ka lapsevanem), kui ei ole siis teeb uue inimese 
        public static Inimene Create(string isikukood)
        {
            if (Inimesed.ContainsKey(isikukood))
                return Inimesed[isikukood];
            else
                return new Inimene(isikukood);
        }
        //sisselogimisel kontrollib isikukoodi alusel inimese olemasolu listis
        public static Inimene ByCode(string isikukood)
        {
            if (Inimesed.ContainsKey(isikukood))
                return Inimesed[isikukood];
            else return null;
        }

        public string Eesnimi;
        public string Perekonnanimi;
        public string Klass = ""; 
        public string Aine = "";
        public string Laps = "";

        static public List<Inimene> Vanemad = new List<Inimene>();
        static public List<Inimene> Opetajad = new List<Inimene>();
        static public List<Inimene> Lapsed = new List<Inimene>();

        public bool KasÕpilane => Klass != "";
        public bool KasÕpetaja => Aine != "";
        public bool KasLapsevanem => Laps != "";
        //public bool KasLapseVanem => Lapsed.Count > 0; // kuidas tunneb ära et tahan konkreetse inimese lapsi???

        public Inimene(string kood = "", string eesnimi = "", string perenimi = "", string klass = "", string aine="", string laps = "")
        {
            Isikukood = kood;
            Eesnimi = eesnimi;
            Perekonnanimi = perenimi;
            Klass = klass;
            Aine = aine;
            Laps = laps;
        }
        public override string ToString()
        {
            return $"{Isikukood}, {Eesnimi}, {Perekonnanimi}, {Klass}, {Aine}, {Laps}";
        }

        //public class Ained
            
        //{
        //    string Aine = File.ReadAllLines(@"../../Kusmida.txt")
        //        .Select(x => x.Trim())
        //        .Where(x => x.Length > 0)
        //        .Select(x => x.Split(','))
        //        .Select(x => new Aine(x[0]) { Eesnimi = x[1], Perekonnanimi = x[2] })
        //        .ToList()
        //        ;

        //}

        //public override string ToString() => $"{(Klass != "" ? Klass + " klassi õpilane"): Aine != "" ? (Aine + "õpetaja") : "" )} {Eesnimi} {Perekonnanimi} ({Isikukood}) {(Lapsed.Count > 0 ? $" {Lapsed.Count } lapse vanem : ")}";

       

    }

    
}
